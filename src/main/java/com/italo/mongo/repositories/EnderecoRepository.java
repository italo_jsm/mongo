package com.italo.mongo.repositories;

import com.italo.mongo.models.Endereco;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EnderecoRepository extends MongoRepository<Endereco, String> {
}
