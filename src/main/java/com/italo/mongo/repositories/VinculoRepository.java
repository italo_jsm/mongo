package com.italo.mongo.repositories;

import com.italo.mongo.models.Vinculo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface VinculoRepository extends MongoRepository<Vinculo, String> {

    @Query("{'usuarioId' : ?0, 'productsId' : ?1}")
    public Vinculo vinculoExiste(String usuarioId, String productsId);
}
