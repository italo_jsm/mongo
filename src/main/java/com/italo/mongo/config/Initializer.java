package com.italo.mongo.config;


import com.italo.mongo.models.Endereco;
import com.italo.mongo.models.Products;
import com.italo.mongo.models.Usuario;
import com.italo.mongo.models.Vinculo;
import com.italo.mongo.repositories.EnderecoRepository;
import com.italo.mongo.repositories.ProductsRepository;
import com.italo.mongo.repositories.UsuarioRepository;
import com.italo.mongo.repositories.VinculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Initializer implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private VinculoRepository vinculoRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
//        Vinculo vinculo = new Vinculo();
//        Usuario u = usuarioRepository.findById("5ddc4f823acddc76f21b8ae3").get();
//        Products products = productsRepository.findById("5ddc4f74cc441338add09d96").get();
//        Vinculo v = vinculoRepository.vinculoExiste("5ddc4f823acddc76f21b8ae3", "5ddc4f74cc441338add09d96");
//        Usuario u = new Usuario();
//        u.setCpf("123456");
//        u.setEmail("asd@asd.coas.es");
//        u.setNomeComposto("Nome");
//        usuarioRepository.save(u);
    }
}
