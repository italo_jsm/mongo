package com.italo.mongo.services;

import com.italo.mongo.models.Vinculo;
import com.italo.mongo.repositories.VinculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VinculoService {

    @Autowired
    private VinculoRepository vinculoRepository;

    public void saveVinculo(Vinculo vinculo){
        try{
            Vinculo v = vinculoRepository.vinculoExiste(vinculo.getUsuarioId(), vinculo.getProductsId());
            Vinculo novoVinculo = new Vinculo();
            novoVinculo.setProductsId(v.getProductsId());
            novoVinculo.setUsuarioId(v.getUsuarioId());
            novoVinculo.setAtivo(true);
            v.setAtivo(false);
            vinculoRepository.save(v);
            vinculoRepository.save(novoVinculo);
        }catch (NullPointerException exception){
            vinculo.setAtivo(true);
            vinculoRepository.save(vinculo);
        }
    }
}
