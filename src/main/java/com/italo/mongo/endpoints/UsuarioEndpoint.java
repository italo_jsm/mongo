package com.italo.mongo.endpoints;

import com.italo.mongo.models.Usuario;
import com.italo.mongo.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(("/usuarios"))
public class UsuarioEndpoint {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @PostMapping
    public ResponseEntity<?> salvaUsuario(@RequestBody Usuario usuario){
        if(usuario.getId() != null)return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(usuarioRepository.save(usuario), HttpStatus.CREATED);
    }

    @PutMapping
    private ResponseEntity<?> editaUsuario(@PathVariable String usuarioId, @RequestBody Usuario aAlterar){
        return new ResponseEntity<>(usuarioRepository.save(aAlterar), HttpStatus.ACCEPTED);
    }

    @GetMapping("/busca-por-id/{id}")
    private ResponseEntity<?> buscaUsuario(@PathVariable String id){
        return new ResponseEntity<>(usuarioRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping
    private ResponseEntity<?> editaUsuario(@PathVariable String id){
        usuarioRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/listar")
    private ResponseEntity<?> listarTodos(){
        return new ResponseEntity<>(usuarioRepository.findAll(), HttpStatus.OK);
    }

}
