package com.italo.mongo.endpoints;

import com.italo.mongo.models.Vinculo;
import com.italo.mongo.services.VinculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vinculos")
public class VinculoEndpoint {

    @Autowired
    private VinculoService vinculoService;

    @PostMapping
    public ResponseEntity<?> criarVinculo(@RequestBody Vinculo vinculo){
        vinculoService.saveVinculo(vinculo);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
